import { createAction } from '@reduxjs/toolkit';

const setMessages = createAction('SET_MESSAGES', messages => ({
    payload: {
        messages
    }
}))

const loadMessages = (url) => async dispatch => {
    const response = await fetch(url);
    //setMessages(await response.json());
    const messages = await response.json();
    console.log(messages);
    dispatch(setMessages(messages));
}

export {
    loadMessages,
    setMessages
};