import { createReducer } from '@reduxjs/toolkit';
import { setMessages } from './actions';

const initialState = {
    messages: []
}

const reducer = createReducer(initialState, builder => {
    builder.addCase(setMessages, (state, action) => {
        const {messages} = action.payload;
        state.messages = messages;
    });
})


export {reducer};