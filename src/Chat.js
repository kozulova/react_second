import {useEffect, useState} from 'react';
import Preloader from './Preloader';
import Header from './Components/Header';
import MessageList from './Components/MessageList';
import MessageInput from './Components/MessageInput';
import { useDispatch, useSelector } from 'react-redux';
import {loadMessages} from './store/actions';

const Chat = ({url}) => {
    //const [messages, setMessages] = useState([]);
    const [loading, setLoading] = useState(true);
    const {messages} = useSelector(state => ({
        messages: state.messages
    }));
    const dispatch = useDispatch();
    useEffect(() => {
        const loadData = async () => {
            //const response = await fetch(url);
            //setMessages(await response.json());
            handleMessagesLoad();
            setLoading(false);
        }
        loadData();
    }, [url])
    const handleMessagesLoad = () => {
        dispatch(loadMessages(url));
      };

    const addMessage = (message) => {
        //setMessages([...messages, message]);
    }
    const deleteMessage = (id) => {
        const newMessages = messages.filter(m=>m.id != id);
        //setMessages(newMessages);
    }

    const likeMessage = (id) => {
        const newMessages = messages.map(m=>{
            if(m.id == id){
                return m
            }else{
                return {...m, likes: m.likes + 1 || 1}
            }
        })
        //setMessages(newMessages);
    }

    return (
        <div className="chat">
        {loading && <Preloader/>}
        <Header messages={messages}/>
        <MessageList messages={messages} deleteMessage={deleteMessage} likeMessage={likeMessage}/>
        <MessageInput addMessage={addMessage}/>
        <div className="copy">Copyright</div>
        </div>
    )
}

export default Chat;