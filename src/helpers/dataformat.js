import moment from 'moment'
export function dateToFromNowDaily( someDate ) {
    let date = moment(someDate);
    if (moment().diff(date, 'days') >= 1) {
        return date.fromNow(); // '2 days ago' etc.
    }
    return date.calendar().split(' ')[0];
}