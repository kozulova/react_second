import React from 'react'
import Message from './Message'
import OwnMessage from './OwnMessage'
import _ from 'lodash'
import moment from 'moment'
import {dateToFromNowDaily} from '../helpers/dataformat'

const MessageList = ({ messages, deleteMessage, likeMessage }) => {
    const currentMessages = messages;
    const groupByDate = () => {
        const dateFormated = currentMessages.map(item => {
            item.day = dateToFromNowDaily(item.createdAt);
            return item;
        })
        const groupedcurrentMessages = _(dateFormated).groupBy('day').value();
        return groupedcurrentMessages;
    }

    const renderMessage = (currentMessagesToRender) => {
        return currentMessagesToRender.map(message => {
            return (message.user === 'Me' ? <OwnMessage message={message} key={message.id} deleteMessage={deleteMessage}/> :
                <Message message={message} key={message.id} likeMessage={likeMessage}/>)
        })
    }
    const renderDivider = (date) => {
        const grouped = groupByDate();
        console.log('render divider', Object.entries(grouped));

        const items = Object.entries(grouped).map((item, index) => {
            const groupcurrentMessages = renderMessage(item[1])
            return (
                <>
                    <div className="currentMessages-divider" key={index}>{item[0]}</div>
                    {groupcurrentMessages}
                </>
            )
        })
        return items.map(item => item)
    }

    return (
        <div className="message-list">
            {renderMessage(currentMessages)}
        </div>
    )
}

export default MessageList
