import React, {useRef} from 'react'

const MessageInput = ({addMessage}) => {
    const textInput = useRef(null);

    const addNewMessage = () => {
        const messageText = textInput.current.value;
        console.log("addNewMessage");
        const message = {
            "id": new Date().getUTCMilliseconds(),
            "userId": 1,
            "avatar": "https://i.pinimg.com/originals/87/07/90/87079055b55e4dab8117f6d580ec92d5.jpg",
            "user": "Me",
            "text": messageText,
            "createdAt": new Date().toISOString(),
        }
        addMessage(message);
        textInput.current.value = '';
    }

    return (
        <div className="message-input">
            <textarea className="message-input-text" name="" id="" ref={textInput}>
            </textarea>
            <button className="message-input-button" onClick={addNewMessage}>Send</button>
        </div>
    )
}

export default MessageInput
