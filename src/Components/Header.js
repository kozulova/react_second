import React from 'react'
import _ from 'lodash'
import moment from 'moment'

const Header = ({messages}) => {
    const curretnMessages = messages;
    const countParticipants = () => {
        return _.uniq(_.map(curretnMessages, 'user')).length;
    }
    const findLastMessage = () => {
        console.log(_.min(_.map(curretnMessages, 'createdAt')));
        return _.max(_.map(curretnMessages, 'createdAt'));
    }
    findLastMessage();
    countParticipants();
    return (
        <div className = 'header'>
            <h2 className = 'header-title'>CHATNAME</h2>
            <div><span className='header-users-count'>{countParticipants()}</span> participants</div>
            <div><span className='header-messages-count'>{curretnMessages.length}</span> messages</div>
            <div>last message at <span className='header-last-message-date'>{moment(findLastMessage()).format("DD.MM.YY HH:MM")}</span></div>
        </div>
    )
}

export default Header
